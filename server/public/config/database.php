<?php

return [
    'driver' => 'mysql',
    'host' => 'imdb_mysql',
    'port' => '3306',
    'database' => 'my_imdb-db',
    'username' => 'admin',
    'password' => 'qwertyZ0ne',
    'charset' => 'utf8',
];