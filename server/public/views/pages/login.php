<?php
/**
 * @var \App\Kernel\View\ViewInterface $view
 * @var \App\Kernel\Session\SessionInterface $session
 */
?>

<?php $view->component('start_simple'); ?>

<main class="form-signin w-100 m-auto">
    <form
        action="?= $_SERVER['SCRIPT_NAME'] ?"
        method="post"
    >
        <?php if ($session->has('error')) { ?>
        <div class="alert alert-danger">
            <?php echo $session->getFlash('error')?>
        </div>
        <?php } ?>
        <div class="d-flex" style="align-items: center; justify-content: space-between">
            <h2>Вхід</h2>
            <a href="/" class="d-flex align-items-center mb-5 mb-lg-0 text-decoration-none">
                <h5 class="m-0">немає часу<span class="badge bg-warning warn__badge">на журбу</span></h5>
            </a>
        </div>
        <div class="form-floating mt-3">
            <input
                type="email"
                class="form-control place"
                name="email"
                id="floatingEmail"
                placeholder="example@mail.com"
            >
            <label for="floatingEmail">E-mail</label>
        </div>
        <div class="form-floating">
            <input
                type="password"
                class="form-control place"
                name="password"
                id="floatingPassword"
                placeholder="*************"
            >
            <label for="floatingPassword">Пароль</label>
        </div>
        <button
            class="btn btn-primary w-100 py-2"
            type="submit"
            autofocus
        >
            Увійти в аккаунт
        </button>
        <p class="mt-5 mb-3 text-center text-body-secondary">&#127279; 2024 Немає часу на журбу</p>
    </form>
</main>

<?php $view->component('end_simple'); ?>
