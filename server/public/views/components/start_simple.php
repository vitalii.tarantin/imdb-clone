<?php
/**
 * @var App\Kernel\View\ViewInterface $view
 */
?>

<!DOCTYPE html>
<html lang="en" data-bs-theme="dark">

<?php $view->component('head'); ?>

<body class="d-flex flex-column min-vh-100">
