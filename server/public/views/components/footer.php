<footer class="py-3 mt-auto">
    <ul class="nav justify-content-center border-bottom pb-3 md-3">
        <li class="nav-item">
            <a href="/" class="nav-link px-2 text-body-secondary">Головна</a>
        </li>
        <li class="nav-item">
            <a href="/best" class="nav-link px-2 text-body-secondary">Краще</a>
        </li>
        <li class="nav-item">
            <a href="/categories" class="nav-link px-2 text-body-secondary">Жанри</a>
        </li>
    </ul>
    <p class="text-center text-body-secondary">&#127279; 2024 Немає часу на журбу</p>
</footer>
