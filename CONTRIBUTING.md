# Contributing

## Versioning

- All notable changes to this project will be documented in [CHANGELOG](CHANGELOG.md) file.
- This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

    ```shell
    MAJOR.MINOR.PATCH
       # MAJOR ---> a breaking change: incompatible changes (ex: reorganization of directories)
       # MINOR ---> add a new feature: a new Docker image (ex: new Asqtasun version)
       # PATCH ---> fix a bug
    ```

> start:
>
> - `git`: https://gitlab.com/vitalii.tarantin/imdb-clone.git).
> - `docker`: one more time docker-compose up --build, next use docker-compose up.
